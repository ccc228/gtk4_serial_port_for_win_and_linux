#include <gtk/gtk.h>
#include "widget_serial_port.h"



static void activate (GtkApplication* app,gpointer user_data)
{
    GtkWidget *window;
    GtkWidget *bt;
    window = gtk_application_window_new (app);

    gtk_window_set_title(GTK_WINDOW(window),"串口调试助手");
    widget_port_init(window);

    gtk_widget_show (window);
    
    GtkCssProvider *css_provider;
    GdkDisplay *display;
    
    css_provider = gtk_css_provider_new();  
    gtk_css_provider_load_from_path(css_provider,"./gtk.css");
    
    display = gdk_display_get_default();
    gtk_style_context_add_provider_for_display(display,GTK_STYLE_PROVIDER(css_provider),GTK_STYLE_PROVIDER_PRIORITY_USER);
}


/*
 *  GTK程序中，mian函数主要目的是创建GtkApplicaation并运行。
 */
int main (int argc,char **argv)
{
  GtkApplication *app;          
  int status;
  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);  //创建GTK应用，第一个参数是应用名称，第二个参数为特殊用途。
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);          //将信号和回调函数绑定
  status = g_application_run (G_APPLICATION (app), argc, argv);             //运行此函数是，activate信号将发送给回调函数,此函数调用main函数的两参数，这两个参数可以程序中应用
  //当关闭应用时，程序将跳出g_application_run,并返回一个整型，
  g_object_unref (app);     //释放内存
  return status;    //返回整型状态，应用程序退出
}
