#ifndef __WIDGET_SERIAL_PORT_H__
#define __WIDGET_SERIAL_PORT_H__


#include <gtk/gtk.h>
#include "serial.h"

typedef struct
{
    int open_flag;
    int boudrate;
    int stopbits;
    int parity;
    int com_select;
    char send_type;
    char rec_type;

    PORT port;

    GtkTextMark* mark;
    GtkWidget* frame_rec;
    GtkWidget* cb_text_mode_rx;
    GtkWidget* cb_hex_mode_rx;
    GtkWidget* bt_clear_rec;
    GtkWidget* vb_rx_mode;
    GtkWidget *scrolled_rec;
    GtkWidget *tv_rec;
    GtkTextBuffer *tb_rec;
    GtkWidget* hb_rec;


    GtkWidget* frame_send;
    GtkWidget* cb_text_mode_tx;
    GtkWidget* cb_hex_mode_tx;
    GtkWidget* bt_clear_tx;
    GtkWidget* vb_tx_mode;
    GtkWidget* scrolled_tx;
    GtkWidget *tv_send;
    GtkTextBuffer *tb_send;
    GtkWidget *hb_send;
    


    GtkWidget *bt_scan;
    GtkWidget *bt_open;
    GtkWidget *bt_send;
    GtkWidget *cbb_baud;
    GtkWidget *cbb_pority;
    GtkWidget *cbb_stopbit;
    GtkWidget *cbb_com;
    GtkWidget* lb_baud;
    GtkWidget* lb_parity;
    GtkWidget* lb_stopbit;

    GtkListStore *list_store_com;
    GtkListStore *list_store_boudrate;
    GtkListStore *list_store_parity;
    GtkListStore *list_store_stopbits;

    //for layout
    GtkWidget *hb_set;
    GtkWidget *vb_body;

    GtkTextMark* rx_mark;
    GtkAdjustment *rx_adj;
    GtkTextIter* rx_iter;

}WIDGET_PORT;

void widget_port_init(GtkWidget* window);
#endif

