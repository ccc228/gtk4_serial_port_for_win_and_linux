SRC = main.c
SRC += widget_serial_port.c
SRC += serial.c


OUTPUT= port

output:$(SRC)
	gcc `pkg-config --cflags gtk4` -g -Ddebug=true -o $(OUTPUT) $(SRC) `pkg-config --libs gtk4`

clean:
	rm $(output)


run:
	./port
