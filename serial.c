#include "serial.h"
#include <stdio.h>
#include <string.h>

#ifdef __WINDOWS__

#else

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <getopt.h>
#include <time.h>
#include <sys/select.h>

#endif


/*
 * idx:端口号，直接填入数字即可，编译区分COMx或ttyUSBx
 *  windows 正常返回句柄，不正常返回NULL
 *  linux 正常返回int,不正常返回-1
 */
PORT serial_port_open(int idx)
{
#ifdef __WINDOWS__
	HANDLE hComm;
	TCHAR comname[100];
	wsprintf(comname,TEXT("COM%d"),idx);
	hComm = CreateFile(comname,
	//hComm = CreateFile(TEXT("COM6"),
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			0,
			NULL);
	if(hComm == INVALID_HANDLE_VALUE)
	{
	//	printf("open 失败\r\n");
		return NULL;
	}
	COMMTIMEOUTS timeouts = {0};
	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier= 10;
	if(SetCommTimeouts(hComm,&timeouts) == FALSE)
	return NULL;

	if(SetCommMask(hComm,EV_RXCHAR) == FALSE)
	return NULL;

	//printf("open %d ok \r\n",idx);
	return hComm;
#else
    int fd;
    char str[20];
    sprintf(str,"/dev/ttyUSB%d",idx);
    //fd = open(str,O_RDWR | O_NOCTTY | O_NDELAY);
    fd = open(str,O_RDWR | O_NOCTTY |O_NONBLOCK);
    return fd;
#endif
    
}


/*
 *  关闭串口
 *  com_port 串口
 */
void serial_port_close(PORT com_port)
{
#ifdef __WINDOWS__
	CloseHandle(com_port);
#else
    close(com_port);
#endif

}




#ifdef __WINDOWS__
int ReciveData(PORT com_port,char *data,int len)
{
	DWORD dwEventMast;
	DWORD NoBytesRead;

	BOOL Status = WaitCommEvent(com_port,&dwEventMast,NULL);
	if(Status == FALSE)
	{
		return FALSE;
	}
	Status = ReadFile(com_port,data,len,&NoBytesRead,NULL);
	data[NoBytesRead] = 0;
	if(Status == FALSE)
		return FALSE;
	else
		printf("%s\n",data);
	return TRUE;
}
#endif


/*
 *  描述:串口发生发送函数
 *  参数:
 *  com_port:端口 
 *  data:要发送的数据
 *  len:要发送的数据长度
 */
int serial_port_send(PORT com_port,const char* data,int len)
{
#ifdef __WINDOWS__
	DWORD dNoOFBytesWriten;
	BOOL Status  = WriteFile(com_port,
			data,
			len,
			&dNoOFBytesWriten,
			NULL);
	if(Status = FALSE)
		return -1;
	else
		printf("send ok\n");
	return 0;
#else
    return write(com_port,data,len);
#endif
}

int serial_port_read(PORT com_port,char * data,int len)
{
#ifdef __WINDOWS__
	DWORD dwEventMask;
	DWORD NoBytesRead;
	BOOL Status;
	//BOOL Status = WaitCommEvent(com_port,&dwEventMask,NULL);
	//if(Status == FALSE)
	//	return -1;
	Status =ReadFile(com_port,data,len,&NoBytesRead,NULL);
	data[NoBytesRead] = 0;
	if(Status == FALSE)
		return -1;
	
	return NoBytesRead;
#else
    return read(com_port,data,len);
#endif
}




/*
 *  说明:linux串口初始化函数
 *  参数:
 *  fd :串口文件号
 *  nSpeed:波特率:
 *      linux支持:2400 4800 9600 19200 38400 57600 115200 230400
 *  nBits:数据位,7或8，其他默认为8
 *  nParity:校验位 0:无校验  1:奇校验  2:偶校验
 *  nStop:停止位  0:1位  1:1位  2:2位  其他:1位
 *
 *  返回:
 *  0正常  -1不正常
 */
int serial_port_config(PORT fd, int nSpeed, int nBits, int nParity, int nStop)
{
#ifdef __WINDOWS__
    
	DCB dcbSerialParams = {0};
	BOOL Status;
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	Status = GetCommState(fd,&dcbSerialParams);
	if(Status == FALSE)
		return FALSE;
    switch(nSpeed)
    {
        case 2400:
	            dcbSerialParams.BaudRate = CBR_2400;
            break;
        case 4800:
	            dcbSerialParams.BaudRate = CBR_4800;
            break;
        case 9600:
	            dcbSerialParams.BaudRate = CBR_9600;
            break;
        case 19200:
	            dcbSerialParams.BaudRate = CBR_19200;
            break;
        case 38400:
	            dcbSerialParams.BaudRate = CBR_38400;
            break;
        case 56000:
	            dcbSerialParams.BaudRate = CBR_56000;
            break;
        case 115200:
	            dcbSerialParams.BaudRate = CBR_115200;
            break;
        default:
	            dcbSerialParams.BaudRate = CBR_9600;
    }
	dcbSerialParams.ByteSize = nBits;
	dcbSerialParams.StopBits = nStop;
	dcbSerialParams.Parity = nParity;
	Status = SetCommState(fd,&dcbSerialParams);

	COMMTIMEOUTS comTimeOut;
	comTimeOut.ReadIntervalTimeout = MAXDWORD;
	comTimeOut.ReadTotalTimeoutMultiplier = 0;
	comTimeOut.ReadTotalTimeoutConstant = 0;

	comTimeOut.WriteTotalTimeoutMultiplier = 0;
	comTimeOut.WriteTotalTimeoutConstant = 0;
	SetCommTimeouts(fd,&comTimeOut);

	return Status;

#else   //linux
    struct termios newtio, oldtio;
    // 保存测试现有串口参数设置，在这里如果串口号等出错，会有相关的出错信息
    if (tcgetattr(fd, &oldtio) != 0)
    {
        perror("SetupSerial 1");
        return -1;
    }

    bzero(&newtio, sizeof(newtio));         //新termios参数清零
    newtio.c_cflag &= ~CRTSCTS;             //不使用硬件流控制
    newtio.c_cflag |= CLOCAL | CREAD;       //CLOCAL--忽略 modem 控制线,本地连线, 不具数据机控制功能, CREAD--使能接收标志

    newtio.c_iflag &= ~(IXON | IXOFF | IXANY);
    newtio.c_oflag &= ~OPOST;       //
                                    //
                                    //
    newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

    // 设置数据位数
    newtio.c_cflag &= ~CSIZE;    //清数据位标志
    switch (nBits)
    {
        case 7:
            newtio.c_cflag |= CS7;
        break;
        case 8:
            newtio.c_cflag |= CS8;
        break;
        default:
            newtio.c_cflag |= CS8;
    }
    // 设置校验位
    switch (nParity)
    {
        case 1:                     
            newtio.c_cflag |= PARENB;   //校验使能
            newtio.c_cflag |= PARODD;   //设置为奇校验
            newtio.c_iflag |= (INPCK | ISTRIP);
            break;
        case 2:                     
            newtio.c_cflag |= PARENB;   
            newtio.c_cflag |= ~PARODD;  //设置为偶校验
            newtio.c_iflag |= (INPCK | ISTRIP);
            break;
            break;
        default:
            newtio.c_cflag &= ~PARENB;
    }
    // 设置停止位,CSTOPB只支持1位或2位
    switch (nStop)
    {
        case 1:
            newtio.c_cflag &= ~CSTOPB;
        break;
        case 2:
            newtio.c_cflag |= CSTOPB;
        break;
        default:
            newtio.c_cflag &= ~CSTOPB;
    }
    // 设置波特率 2400/4800/9600/19200/38400/57600/115200/230400
    switch (nSpeed)
    {
        case 2400:
            cfsetispeed(&newtio, B2400);
            cfsetospeed(&newtio, B2400);
            break;
        case 4800:
            cfsetispeed(&newtio, B4800);
            cfsetospeed(&newtio, B4800);
            break;
        case 9600:
            cfsetispeed(&newtio, B9600);
            cfsetospeed(&newtio, B9600);
            break;
        case 19200:
            cfsetispeed(&newtio, B19200);
            cfsetospeed(&newtio, B19200);
            break;
        case 38400:
            cfsetispeed(&newtio, B38400);
            cfsetospeed(&newtio, B38400);
            break;
        case 57600:
            cfsetispeed(&newtio, B57600);
            cfsetospeed(&newtio, B57600);
            break;
        case 115200:
            cfsetispeed(&newtio, B115200);
            cfsetospeed(&newtio, B115200);
            break;
        case 230400:
            cfsetispeed(&newtio, B230400);
            cfsetospeed(&newtio, B230400);
            break;
        default:
            printf("\tSorry, Unsupported baud rate, set default 9600!\n\n");
            cfsetispeed(&newtio, B9600);
            cfsetospeed(&newtio, B9600);
            break;
    }
    // 设置read读取最小字节数和超时时间
    newtio.c_cc[VTIME] = 100;         // 读取一个字符等待1*(1/10)s
    newtio.c_cc[VMIN] = 1;          // 读取字符的最少个数为1

    tcflush(fd,TCIFLUSH);         //清空缓冲区
    if (tcsetattr(fd, TCSANOW, &newtio) != 0)    //激活新设置
    {
         perror("SetupSerial 3");
         return -1;
    }
    return 0;
#endif
}

/*  
 *  描述:串口初始化，初始化完成后串口是打开的
 *  参数:
 *  idx:串口号，0-n,linux windows 通用
 *  rate:波特率，直接输入即可。如9600，19200，115200,对于不支持的波特率都会默认设置为9600
 *  databits:数据位，直接写8就好了 4-8,对里linux只支持7和8
 *  stopbits:停止位 0:1.5位  1:1位  2:2位 其他默认位1位
 *  parity:校验位:0无校验  1奇校验  2偶校验
 *  
 */
PORT serial_port_init(int idx,int rate,int databits,int stopbits,int parity)
{
	int ret = 0;
	PORT com_port;
	com_port = serial_port_open(idx);
//打开串口
#ifdef __WINDOWS__
	if(com_port == INVALID_HANDLE_VALUE)
	{
		return NULL;
	}
#else
    //linux
	if(com_port == -1)
	{
		return -1;
	}
#endif
    
serial_port_config(com_port,rate,8,parity,stopbits);
	return com_port;
}
