#ifndef __SERIAL_H__
#define __SERIAL_H__

//选择定义来编译
//#define __WINDOWS__




#ifdef __WINDOWS__
#include<windows.h>
typedef HANDLE PORT;
#else
typedef int PORT;
#endif

PORT serial_port_init(int idx,int rate,int databits,int stopbits,int parity);

PORT serial_port_open(int idx);
void serial_port_close(PORT com_port);

int serial_port_send(PORT com_port,const char *data,int len);
int serial_port_read(PORT com_port,char * data,int len);

#endif
