#include "widget_serial_port.h"
#include "serial.h"
#include <stdint.h>
#include <pthread.h>

#ifdef __WINDOWS__
#include <windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif


//轮询需要用到的串口号最大值
#define SCAN_COM_MAX    16

WIDGET_PORT w_port;
char buff[1024] = {0};
int rev_len = 0;
int useable_com[SCAN_COM_MAX];

pthread_mutex_t mutex;
/*
 *  描述:扫描可用串口,并将对应串口添加到combox上显示
 */
static void scan_port(void)
{
    PORT com;
    GtkTreeIter iter;
    GtkCellRenderer *renderer;
    int i;
    PORT port;
    char com_name[16];
    int port_sum = 0;
    gtk_list_store_clear(w_port.list_store_com);
    for(i=0;i<SCAN_COM_MAX;i++)
    {
        com = serial_port_open(i);
#ifdef __WINDOWS__
        if(com == NULL)
        {
        //printf("串口打开失败");
        }
#else
        if(com == -1)
        {
        //printf("串口打开失败");
        }
#endif
        else
        {
            useable_com[port_sum] = i;
            port_sum ++;

            gtk_list_store_append(w_port.list_store_com,&iter);
#ifdef __WINDOWS__
//windows
            sprintf(com_name,"COM%d",i);
#else
//linux
            sprintf(com_name,"/dev/ttyUSB%d",i);
#endif
            gtk_list_store_set(w_port.list_store_com,&iter,0,com_name,-1);
            serial_port_close(com);
        }
    }
    if(port_sum != 0)
    {
            gtk_combo_box_set_active(GTK_COMBO_BOX(w_port.cbb_com),0);
    }
}

const char num[]="0123456789ABCDEF";

/*
 *  按键处理
 */
void bt_handle(GtkButton* bt,gpointer param)
{
    //扫描串口
    if(bt == GTK_BUTTON(w_port.bt_scan))
    {
        if(w_port.open_flag == 0)
        {
            scan_port();
        }
    }
    //打开串口
    else if(bt == GTK_BUTTON(w_port.bt_open))
    {
        if(w_port.open_flag == 1)
        {
            w_port.open_flag = 0;
            serial_port_close(w_port.port);   
            gtk_button_set_label(GTK_BUTTON(w_port.bt_open),"打开串口");
        }
        else
        {
            int num;
            char str[20];

            char* serial_com;
            num  = gtk_combo_box_get_active(GTK_COMBO_BOX(w_port.cbb_com));
            sprintf(str,"%d",num);
    
            if(num == -1)
            {
                printf("no port finded\r\n");
                return;
            }
    
            GtkTreeIter iter;
            gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(w_port.list_store_com),&iter,str);//获得combobox iter
            gtk_tree_model_get(GTK_TREE_MODEL(w_port.list_store_com),&iter,     //获得iter对应串口名称
                        0,&serial_com,
                        -1);
            int com_name_len;
            com_name_len = strlen(serial_com);
            //printf("com name is %s \r\n",serial_com);
            //printf("serial com name size %d\r\n",com_name_len);
    
    #ifdef __WINDOWS__
            if(com_name_len == 4)
            {
                   
            }
            else if(com_name_len == 5)
            {
    
            }
    #else
            if(com_name_len == 12)
            {
                char str_com_name[13];
                int i;
                for(i=0;i<10;i++)
                {
                   sprintf(str_com_name,"/dev/ttyUSB%d",i);
                   if(memcmp(serial_com,str_com_name,12) == 0)
                   {
                        //printf("com dective %s *****************\r\n",str_com_name);
                        break;
                   }
                }
            }
            else if(com_name_len == 13)
            {
    
            }
    #endif
    
            //存在可用串口
            if(num != -1)
            {
                sprintf(str,"%d",num);
                //printf("port is %d\r\n",num);
                //printf("str is %s\r\n",str);
    
                char* com_str;
                gboolean valid;
    
                valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(w_port.list_store_com),&iter);
                if(valid != TRUE)
                {
                    //printf("get iter false \r\n");
                }
    
                gtk_tree_model_get(GTK_TREE_MODEL(w_port.list_store_com),&iter,
                        0,&com_str,
                        -1);
                //printf("com select is %s\r\n",com_str);
    
                //配置停止位*************************************************************
                char *stopbits_str;
                int stop = gtk_combo_box_get_active(GTK_COMBO_BOX(w_port.cbb_stopbit));
                sprintf(str,"%d",stop);
                gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(w_port.list_store_stopbits),&iter,str);
                gtk_tree_model_get(GTK_TREE_MODEL(w_port.list_store_stopbits),&iter,
                        0,&stopbits_str,
                        -1);
                if(memcmp(stopbits_str,"1.5",sizeof("1.5")) == 0)
                {
                    //printf("1.5\n");
                    stop = 0;
                }
                else if(memcmp(stopbits_str,"1",sizeof("1")) == 0)
                {
                    //printf("1\n");
                    stop = 1;
                }
                else if(memcmp(stopbits_str,"2",sizeof("2")) == 0)
                {
                    //printf("2\n");
                    stop = 2;
                }
                //获得校验方式*********************************************************
                char *parity_str;
                int parity = gtk_combo_box_get_active(GTK_COMBO_BOX(w_port.cbb_pority));
                sprintf(str,"%d",parity);
                gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(w_port.list_store_parity),&iter,str);
                gtk_tree_model_get(GTK_TREE_MODEL(w_port.list_store_parity),&iter,
                        0,&parity_str,
                        -1);
                if(memcmp(parity_str,"无校验",sizeof("无校验")) == 0)
                {
                    //printf("无校验\n");
                    parity = 0;
                }
                else if(memcmp(parity_str,"奇校验",sizeof("奇校验")) == 0)
                {
                    //printf("奇校验\n");
                    parity = 1;
                }
                else if(memcmp(parity_str,"偶校验",sizeof("偶校验")) == 0)
                {
                    //printf("偶校验\n");
                    parity = 2;
                }
                //获得波特率*************************************************************
                int boudrate = gtk_combo_box_get_active(GTK_COMBO_BOX(w_port.cbb_baud));
                sprintf(str,"%d",boudrate);
                gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(w_port.list_store_boudrate),&iter,str);
                gtk_tree_model_get(GTK_TREE_MODEL(w_port.list_store_boudrate),&iter,
                        0,&boudrate,
                        -1);
                //串口配置
                w_port.port = serial_port_init(useable_com[num],boudrate,8,stop,parity);
    #ifdef __WINDOWS__
                if(w_port.port!= NULL)
    #else
                if(w_port.port != -1)
    #endif
                {
                    w_port.open_flag = 1;
                    gtk_button_set_label(GTK_BUTTON(w_port.bt_open),"关闭串口");
                    //serial_port_close(w_port.port);
                }
            }
        }           
    }
    //清楚接受缓存
    else if(bt == GTK_BUTTON(w_port.bt_clear_rec))
    {
        printf("clear receive\r\n");
        GtkTextIter iter_end;
        GtkTextIter iter_start;
        gtk_text_buffer_get_end_iter(w_port.tb_rec,&iter_end);
        gtk_text_buffer_get_start_iter(w_port.tb_rec,&iter_start);
        gtk_text_buffer_delete(w_port.tb_rec,&iter_start,&iter_end);
    }
    //发送数据
    else if(bt == GTK_BUTTON(w_port.bt_send))
    {
        if(w_port.open_flag == 1)
        {
            GtkTextIter iter_end;
            GtkTextIter iter_start;
            gtk_text_buffer_get_end_iter(w_port.tb_send,&iter_end);
            gtk_text_buffer_get_start_iter(w_port.tb_send,&iter_start);
            
            char* txbuf;
            txbuf = gtk_text_buffer_get_text(w_port.tb_send,&iter_start,&iter_end,TRUE);
            if(w_port.send_type == 1)
            {
                serial_port_send(w_port.port,txbuf,strlen(txbuf));
            }
            else
            {
                int i;
                int len = strlen(txbuf);
                unsigned char* hex_tx;
                hex_tx = malloc(len);

                int sum = 0;
                int hex_cnt = 0;
                unsigned char hex_byte[2];
                unsigned char hex_h;
                unsigned char hex_l;
                printf("len is %d\n",len);
                for(i=0;i<len;i++)
                {

                    if((txbuf[i] != ' ') && (i != (len - 1)))
                    {
                        hex_byte[sum] = txbuf[i];
                        sum ++;
                    }
                    //一个字符接收结束
                    else
                    {
                        if((i == len - 1) &&(txbuf[i] != ' '))
                        {
                            hex_byte[sum] = txbuf[i];
                            sum ++;
                        }
                        char flag = 0;
                        //正常hex数据
                        if((sum > 0)&& (sum <3))
                        {
                            if((hex_byte[0] >= '0')&&(hex_byte[0] <= '9'))
                            {
                                hex_h = hex_byte[0] - '0';
                            }
                            else if((hex_byte[0] >= 'a') && (hex_byte[0] <= 'f')) 
                            {
                                hex_h = hex_byte[0] -  'a' + 0x0a;
                            }
                            else if((hex_byte[0] >= 'A') && (hex_byte[0] <= 'F')) 
                            {
                                hex_h = hex_byte[0] - 'A' + 0x0a;
                            }
                            else
                            {
                                flag = 1;
                            }
                            
                            if(sum > 1)
                            {
                                if((hex_byte[1] >= '0')&&(hex_byte[1] <= '9'))
                                {
                                    hex_l = hex_byte[1] - '0';
                                }
                                else if((hex_byte[1] >= 'a') && (hex_byte[1] <= 'f')) 
                                {
                                    hex_l = hex_byte[1] -  'a' + 0x0a;
                                }
                                else if((hex_byte[1] >= 'A') && (hex_byte[1] <= 'F')) 
                                {
                                    hex_l = hex_byte[1] -  'A' + 0x0a;
                                }
                                else
                                {
                                    flag = 1;
                                }
                            }

                            if(flag == 0)
                            {
                                unsigned char tmp;
                                if(sum == 1)
                                {
                                    *(hex_tx + hex_cnt) = hex_h;
                                }
                                else
                                {
                                    *(hex_tx + hex_cnt) = (hex_h<<4)|(hex_l);
                                    tmp = (hex_h<<4)|(hex_l);
                                }

                                hex_cnt ++;
                                printf("check out %x\n",tmp);
                            }
                            else
                            {
                                printf("error0\n");

                            }

                        }
                        //错误数据丢掉
                        else
                        {
                            printf("hex len error\n");
                        }
                        sum = 0;
                    }
                }
                serial_port_send(w_port.port,(char*)hex_tx,hex_cnt);
                printf("error hex num %d\n",hex_cnt);
                free(hex_tx);
            }
        }
    }
}

/*
 *
 */
void cb_check_button(GtkCheckButton* cbt,gpointer dat)
{
    w_port.rec_type = gtk_check_button_get_active(cbt);
    printf("rec change %d \n",w_port.rec_type);
}
void cb_check_button_tx(GtkCheckButton* cbt,gpointer dat)
{
    w_port.send_type = gtk_check_button_get_active(cbt);
    printf("send change %d \n",w_port.send_type);
}


void signal_init(void)
{
    g_signal_connect(w_port.bt_open,"clicked",G_CALLBACK(bt_handle),NULL);
    g_signal_connect(w_port.bt_clear_rec,"clicked",G_CALLBACK(bt_handle),NULL);
    g_signal_connect(w_port.bt_scan,"clicked",G_CALLBACK(bt_handle),NULL);
    g_signal_connect(w_port.bt_send,"clicked",G_CALLBACK(bt_handle),NULL);

    g_signal_connect(w_port.cb_text_mode_rx,"toggled",G_CALLBACK(cb_check_button),NULL);
    g_signal_connect(w_port.cb_text_mode_tx,"toggled",G_CALLBACK(cb_check_button_tx),NULL);
}


void widget_data_init(void)
{
    w_port.open_flag = 0;
    w_port.boudrate = 0;
    w_port.parity = 0;
    w_port.stopbits = 0;
    w_port.com_select = 0;

    w_port.rec_type = 1;
    w_port.send_type = 1;
}

void cb_adj_change(GtkAdjustment* adj,gpointer param)
{
    printf("adj change\n");
}

static void widget_init(void)
{
    //接收缓冲区*************************************************************
    w_port.frame_rec = gtk_frame_new("接收缓冲区");
    w_port.cb_text_mode_rx = gtk_check_button_new_with_label("字符模式");
    w_port.cb_hex_mode_rx = gtk_check_button_new_with_label("HEX模式");
    gtk_check_button_set_group(GTK_CHECK_BUTTON(w_port.cb_text_mode_rx),GTK_CHECK_BUTTON(w_port.cb_hex_mode_rx));
    gtk_check_button_set_active(GTK_CHECK_BUTTON(w_port.cb_text_mode_rx),1);
    w_port.bt_clear_rec= gtk_button_new_with_label("清除");
    w_port.vb_rx_mode = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_widget_set_name(w_port.vb_rx_mode,"box_mode");
    gtk_box_append(GTK_BOX(w_port.vb_rx_mode),w_port.cb_text_mode_rx);
    gtk_box_append(GTK_BOX(w_port.vb_rx_mode),w_port.cb_hex_mode_rx);
    gtk_box_append(GTK_BOX(w_port.vb_rx_mode),w_port.bt_clear_rec);
    
    w_port.tv_rec = gtk_text_view_new();
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(w_port.tv_rec),GTK_WRAP_CHAR);
    gtk_widget_set_hexpand(w_port.tv_rec,true);
    gtk_widget_set_vexpand(w_port.tv_rec,true);
    //gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(w_port.tv_rec),GTK_WRAP_CHAR);    //显示字符换行


    w_port.tb_rec = gtk_text_view_get_buffer(GTK_TEXT_VIEW(w_port.tv_rec));
    GtkTextIter iiter;
    gtk_text_buffer_get_end_iter(w_port.tb_rec, &iiter);
    w_port.rx_mark = gtk_text_buffer_create_mark(w_port.tb_rec,NULL,&iiter,FALSE);

    //w_port.rx_adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(w_port.scrolled_rec));
    //g_signal_connect(w_port.rx_adj,"changed",G_CALLBACK(cb_adj_change),NULL);

    //gtk_text_buffer_insert(w_port.tb_rec,&iiter,"0123456789",10);
    //gtk_text_buffer_add_mark(w_port.tb_rec,w_port.rx_mark,w_port.rx_iter);
    GtkTextIter textiter;
    //w_port.mark = gtk_text_buffer_create_mark(GTK_TEXT_BUFFER(w_port.tb_rec),"aa",&textiter,true);

    w_port.scrolled_rec = gtk_scrolled_window_new();
    gtk_scrolled_window_set_child(GTK_SCROLLED_WINDOW(w_port.scrolled_rec),w_port.tv_rec);

    gtk_scrolled_window_set_propagate_natural_height(GTK_SCROLLED_WINDOW(w_port.scrolled_rec),true);

    w_port.hb_rec = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
    gtk_box_append(GTK_BOX(w_port.hb_rec),w_port.vb_rx_mode);
    gtk_box_append(GTK_BOX(w_port.hb_rec),w_port.scrolled_rec);
    
    gtk_frame_set_child(GTK_FRAME(w_port.frame_rec),w_port.hb_rec);
    //发送缓冲区**************************************************************
    w_port.frame_send= gtk_frame_new("发送缓冲区");
    w_port.cb_text_mode_tx = gtk_check_button_new_with_label("字符模式");
    w_port.cb_hex_mode_tx = gtk_check_button_new_with_label("HEX模式");
    gtk_check_button_set_group(GTK_CHECK_BUTTON(w_port.cb_text_mode_tx),GTK_CHECK_BUTTON(w_port.cb_hex_mode_tx));
    gtk_check_button_set_active(GTK_CHECK_BUTTON(w_port.cb_text_mode_tx),1);
    w_port.bt_clear_tx= gtk_button_new_with_label("清除");
    w_port.vb_tx_mode = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_widget_set_name(w_port.vb_tx_mode,"box_mode");
    gtk_box_append(GTK_BOX(w_port.vb_tx_mode),w_port.cb_text_mode_tx);
    gtk_box_append(GTK_BOX(w_port.vb_tx_mode),w_port.cb_hex_mode_tx);
    gtk_box_append(GTK_BOX(w_port.vb_tx_mode),w_port.bt_clear_tx);

    w_port.tv_send = gtk_text_view_new();
    gtk_widget_set_hexpand(w_port.tv_send,1);
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(w_port.tv_send),GTK_WRAP_CHAR);    //显示字符换行
    w_port.tb_send = gtk_text_view_get_buffer(GTK_TEXT_VIEW(w_port.tv_send));

    w_port.scrolled_tx = gtk_scrolled_window_new();
    gtk_scrolled_window_set_child(GTK_SCROLLED_WINDOW(w_port.scrolled_tx),w_port.tv_send);

    w_port.hb_send = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
    gtk_box_append(GTK_BOX(w_port.hb_send),w_port.vb_tx_mode);
    gtk_box_append(GTK_BOX(w_port.hb_send),w_port.scrolled_tx);

    gtk_frame_set_child(GTK_FRAME(w_port.frame_send),w_port.hb_send);

    //串口基本配置************************************************************
    GtkTreeIter iter;
    GtkCellRenderer *renderer;
    GtkListStore* list_store;

    w_port.cbb_com= gtk_combo_box_new();
    w_port.list_store_com = gtk_list_store_new(1,G_TYPE_STRING);
    gtk_combo_box_set_model(GTK_COMBO_BOX(w_port.cbb_com),GTK_TREE_MODEL(w_port.list_store_com));
    renderer = gtk_cell_renderer_text_new();
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(w_port.cbb_com),renderer,TRUE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(w_port.cbb_com),renderer,"text",0,NULL);
    gtk_combo_box_set_active(GTK_COMBO_BOX(w_port.cbb_com),0);

    w_port.bt_scan = gtk_button_new_with_label("扫描");
    w_port.lb_baud = gtk_label_new("波特率");

    //波特率初始化
    w_port.cbb_baud = gtk_combo_box_new();
    w_port.list_store_boudrate= gtk_list_store_new(1,G_TYPE_INT);
    gtk_list_store_append(w_port.list_store_boudrate,&iter);
    gtk_list_store_set(w_port.list_store_boudrate,&iter,0,9600,-1);
    gtk_list_store_append(w_port.list_store_boudrate,&iter);
    gtk_list_store_set(w_port.list_store_boudrate,&iter,0,19200,-1);
    gtk_list_store_append(w_port.list_store_boudrate,&iter);
    gtk_list_store_set(w_port.list_store_boudrate,&iter,0,115200,-1);
    gtk_combo_box_set_model(GTK_COMBO_BOX(w_port.cbb_baud),GTK_TREE_MODEL(w_port.list_store_boudrate));
    renderer = gtk_cell_renderer_text_new();
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(w_port.cbb_baud),renderer,TRUE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(w_port.cbb_baud),renderer,"text",0,NULL);
    gtk_combo_box_set_active(GTK_COMBO_BOX(w_port.cbb_baud),0);



    //波特率初始化
    w_port.lb_parity = gtk_label_new("校验位");
    w_port.cbb_pority = gtk_combo_box_new();

    w_port.list_store_parity = gtk_list_store_new(1,G_TYPE_STRING);
    gtk_list_store_append(w_port.list_store_parity,&iter);
    gtk_list_store_set(w_port.list_store_parity,&iter,0,"无校验",-1);
    gtk_list_store_append(w_port.list_store_parity,&iter);
    gtk_list_store_set(w_port.list_store_parity,&iter,0,"奇校验",-1);
    gtk_list_store_append(w_port.list_store_parity,&iter);
    gtk_list_store_set(w_port.list_store_parity,&iter,0,"偶校验",-1);
    gtk_combo_box_set_model(GTK_COMBO_BOX(w_port.cbb_pority),GTK_TREE_MODEL(w_port.list_store_parity));
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(w_port.cbb_pority),renderer,TRUE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(w_port.cbb_pority),renderer,"text",0,NULL);
    gtk_combo_box_set_active(GTK_COMBO_BOX(w_port.cbb_pority),0);

    //停止位
    w_port.lb_stopbit = gtk_label_new("停止位");
    w_port.cbb_stopbit = gtk_combo_box_new();

    w_port.list_store_stopbits = gtk_list_store_new(1,G_TYPE_STRING);
    gtk_list_store_append(w_port.list_store_stopbits,&iter);
    gtk_list_store_set(w_port.list_store_stopbits,&iter,0,"1",-1);
    gtk_list_store_append(w_port.list_store_stopbits,&iter);
    gtk_list_store_set(w_port.list_store_stopbits,&iter,0,"1.5",-1);
    gtk_list_store_append(w_port.list_store_stopbits,&iter);
    gtk_list_store_set(w_port.list_store_stopbits,&iter,0,"2",-1);
    gtk_combo_box_set_model(GTK_COMBO_BOX(w_port.cbb_stopbit),GTK_TREE_MODEL(w_port.list_store_stopbits));
    renderer = gtk_cell_renderer_text_new();
    gtk_cell_layout_pack_start(GTK_CELL_LAYOUT(w_port.cbb_stopbit),renderer,TRUE);
    gtk_cell_layout_set_attributes(GTK_CELL_LAYOUT(w_port.cbb_stopbit),renderer,"text",0,NULL);
    gtk_combo_box_set_active(GTK_COMBO_BOX(w_port.cbb_stopbit),0);

    w_port.bt_open = gtk_button_new_with_label("打开");

    w_port.hb_set = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);

    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.cbb_com);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.bt_scan);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.lb_baud);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.cbb_baud);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.lb_parity);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.cbb_pority);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.lb_stopbit);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.cbb_stopbit);
    gtk_box_append(GTK_BOX(w_port.hb_set),w_port.bt_open);

    //
    w_port.bt_send = gtk_button_new_with_label("发送");


    w_port.vb_body = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);



    //总布局
    gtk_box_append(GTK_BOX(w_port.vb_body),w_port.frame_rec);
    gtk_box_append(GTK_BOX(w_port.vb_body),w_port.frame_send);
    gtk_box_append(GTK_BOX(w_port.vb_body),w_port.bt_send);
    gtk_box_append(GTK_BOX(w_port.vb_body),w_port.hb_set);
}

const char hex[]="0123456789ABCDEF";

gboolean update_disp(gpointer param)
{
    GtkAdjustment* adj;
    adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(w_port.scrolled_rec));
    gtk_adjustment_set_value(adj,gtk_adjustment_get_upper(adj));
    return 0;
}

gboolean cb_loop(gpointer param)
{
    int len = 0;
    unsigned char buf[4024];
    unsigned char hex_buf[4096];
    int i;

    uint32_t rx_len = 0;
    int fd;
    GtkTextIter iter;
    if(w_port.open_flag != 0)
    {
        len = serial_port_read(w_port.port,(char*)buf,1024);
        if(len != -1)
        {
            rx_len += len;
            //十六进制显示
            if(w_port.rec_type == 0)
            {
                for(i=0;i<len;i++)
                {
                    hex_buf[i*3 + 0] = hex[(buf[i] >> 4)];
                    hex_buf[i*3 + 1] = hex[buf[i]&0xf];
                    hex_buf[i*3 + 2] = ' ';
                }
                gtk_text_buffer_get_iter_at_mark(w_port.tb_rec,&iter,w_port.rx_mark);
                gtk_text_buffer_insert(w_port.tb_rec,&iter,(char*)hex_buf,len * 3);
            }
            //字符显示
            else
            {
                gtk_text_buffer_get_iter_at_mark(w_port.tb_rec,&iter,w_port.rx_mark);
                gtk_text_buffer_insert(w_port.tb_rec,&iter,(char*)buf,len);
            }
            g_timeout_add(100,update_disp,NULL);//启动定时器，延时将scrolled_window 滚到最下边
        }
    }
    g_timeout_add(100,cb_loop,NULL);    //启动定时器
    return 0;
}



void widget_port_init(GtkWidget* window)
{
    widget_data_init();     //数据初始化
    widget_init();          //widget布局等初始化
    gtk_window_set_child(GTK_WINDOW(window),w_port.vb_body);
    signal_init();          //信号槽初始化
    scan_port();            //扫面一遍可用串口

    guint loop;
    loop = g_timeout_add(1000,cb_loop,NULL);    //启动定时器，用于循环读取串口数据
}


